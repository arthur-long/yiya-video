package com.yiya.video;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YiYaVideoBoot {
    public static void main(String[] args) {
        SpringApplication.run(YiYaVideoBoot.class);
    }
}
