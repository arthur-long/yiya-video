package com.yiya.video.modal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class VideoDetail {
    private int vod_id;
    private int type_id;
    private int type_id_1;
    private int group_id;
    private String vod_name;
    private String vod_sub;
    private String vod_en;
    private int vod_status;
    private String vod_letter;
    private String vod_color;
    private String vod_tag;
    private String vod_class;
    private String vod_pic;
    private String vod_pic_thumb;
    private String vod_pic_slide;
    private String vod_actor;
    private String vod_director;
    private String vod_writer;
    private String vod_behind;
    private String vod_blurb;
    private String vod_remarks;
    private String vod_pubdate;
    private int vod_total;
    private String vod_serial;
    private String vod_tv;
    private String vod_weekday;
    private String vod_area;
    private String vod_lang;
    private String vod_year;
    private String vod_version;
    private String vod_state;
    private String vod_author;
    private String vod_jumpurl;
    private String vod_tpl;
    private String vod_tpl_play;
    private String vod_tpl_down;
    private int vod_isend;
    private int vod_lock;
    private int vod_level;
    private int vod_copyright;
    private int vod_points;
    private int vod_points_play;
    private int vod_points_down;
    private int vod_hits;
    private int vod_hits_day;
    private int vod_hits_week;
    private int vod_hits_month;
    private String vod_duration;
    private int vod_up;
    private int vod_down;
    private String vod_score;
    private int vod_score_all;
    private int vod_score_num;
    private Date vod_time;
    private long vod_time_add;
    private int vod_time_hits;
    private int vod_time_make;
    private int vod_trysee;
    private int vod_douban_id;
    private String vod_douban_score;
    private String vod_reurl;
    private String vod_rel_vod;
    private String vod_rel_art;
    private String vod_pwd;
    private String vod_pwd_url;
    private String vod_pwd_play;
    private String vod_pwd_play_url;
    private String vod_pwd_down;
    private String vod_pwd_down_url;
    private String vod_content;
    private String vod_play_from;
    private String vod_play_server;
    private String vod_play_note;
    private String vod_play_url;
    private List<String> vod_play_url_list;
    private String vod_down_from;
    private String vod_down_server;
    private String vod_down_note;
    private String vod_down_url;
    private String type_name;
}
