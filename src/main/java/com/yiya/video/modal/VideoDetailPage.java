package com.yiya.video.modal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class VideoDetailPage {
    private int code;
    private String msg;
    private int page;
    private int pagecount;
    private String limit;
    private int total;
    private List<VideoDetail> list;
}
