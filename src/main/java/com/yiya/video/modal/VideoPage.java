package com.yiya.video.modal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 有class属性需要单独处理
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class VideoPage {
    private int code;
    private String msg;
    private int page;
    private int pagecount;
    private String limit;
    private int total;
    private List<LastVideo> list;
    private List<Subject> clz;
}
