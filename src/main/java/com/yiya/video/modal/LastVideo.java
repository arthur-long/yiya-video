package com.yiya.video.modal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LastVideo {
    private String vod_id;
    private String vod_name;
    private String type_id;
    private String type_name;
    private String vod_en;
    private Date vod_time;
    private String vod_remarks;
    private String vod_play_from;
}
