package com.yiya.video.modal;

public class CodeMsg {

    private int code;
    private String msg;
    private Object obj;

    public static CodeMsg SERVER_ERROR = new CodeMsg(500, "服务端异常");

    public static CodeMsg APP_ERROR(Object obj){
        return new CodeMsg(500, "应用异常",obj);
    }

    private CodeMsg(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    private CodeMsg(int code, String msg,Object obj) {
        this.code = code;
        this.msg = msg;
        this.obj=obj;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

}