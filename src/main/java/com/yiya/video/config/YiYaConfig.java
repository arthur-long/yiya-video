package com.yiya.video.config;

public class YiYaConfig {

    public static String DO_MAIN="https://api.okzy.tv";
    public static String  AC_LIST="/api.php/provide/vod/?ac=list";
    public static String AC_INFO="/api.php/provide/vod/?ac=detail";
    public static String ART_INFO="/api.php/provide/vod/?art=detail";
    public static String ART_LIST="/api.php/provide/vod/?art=list";

    public static String PARM_TYPE ="&t=";
    public static String PARM_PG = "&pg=";
    public static String PARM_WD ="&wd=";
    public static String PARM_IDS ="&ids=";

}
