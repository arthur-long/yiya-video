package com.yiya.video.init;

import com.yiya.video.service.ISysExcuteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class YiYaCommandLine implements CommandLineRunner {
    @Autowired
    private ISysExcuteService sysExcuteService;

    @Override
    public void run(String... args) throws Exception {
        sysExcuteService.excuteVieo();
    }
}
