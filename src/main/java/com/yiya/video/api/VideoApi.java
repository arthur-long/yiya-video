package com.yiya.video.api;

import cn.hutool.core.net.URLEncoder;
import cn.hutool.core.text.UnicodeUtil;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.yiya.video.config.YiYaConfig;
import com.yiya.video.modal.*;
import com.yiya.video.service.IvideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * video api接口
 */
@RequestMapping("/video/api")
@RestController
public class VideoApi {

    @Autowired
    private IvideoService videoService;

    @GetMapping("/subjects")
    public Result subjects(){
        try {
            String subJect = videoService.getSubJect();
            return Result.success(subJect);
        }catch (Exception e){
            return Result.error(CodeMsg.APP_ERROR(e.getMessage()));
        }
    }

    /**
     * 搜索视频
     * @param wd 关键字
     * @return
     */
    @GetMapping("/query/video/{wd}")
    public Result videos(@PathVariable("wd") String wd){
        try {
            List<LastVideo> searchVideo = videoService.getSearchVideo(wd);
            return Result.success(searchVideo);
        }catch (Exception e){
            return Result.error(CodeMsg.APP_ERROR(e.getMessage()));
        }
    }
    /**
     * 获取视频详情
     * @param id 视频ID
     * @return
     */
    @GetMapping("/video/{id}")
    public Result videoDetails(@PathVariable("id") String id){
        try {
            List<VideoDetail> videoDetail = videoService.getVideoDetail(id);
            return Result.success(videoDetail);
        }catch (Exception e){
            return Result.error(CodeMsg.APP_ERROR(e.getMessage()));
        }
    }

    /**
     * 通过typeId搜索视频
     * @param type_id 关键字
     * @return
     */
    @GetMapping("/query/list/{type_id}")
    public Result videoList(@PathVariable("type_id") String type_id){
        try {
            List<LastVideo> searchVideo = videoService.getSearchVideoByTypeId(type_id);
            return Result.success(searchVideo);
        }catch (Exception e){
            return Result.error(CodeMsg.APP_ERROR(e.getMessage()));
        }
    }
}
