package com.yiya.video.service.impl;

import cn.hutool.core.text.UnicodeUtil;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONUtil;
import com.yiya.video.config.YiYaConfig;
import com.yiya.video.modal.Subject;
import com.yiya.video.modal.VideoPage;
import com.yiya.video.service.ISysExcuteService;
import com.yiya.video.utill.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 系统处理类
 */
@Slf4j
@Service
public class SysExcuteServiceImpl implements ISysExcuteService {

    @Autowired
    private RedisUtil redisUtil;

    @Override
    public void excuteVieo() {
        String data = HttpUtil.get(YiYaConfig.DO_MAIN + YiYaConfig.AC_LIST);
        String res = UnicodeUtil.toString(data);
        VideoPage videoPage = JSONUtil.toBean(res, VideoPage.class);
        List<Subject> sub = (List<Subject> )JSONUtil.parseObj(res).get("class");
        videoPage.setClz(sub);
        excuteSubject(sub);
        log.info("视频采集初始化完成!");
    }

    private void excuteSubject(List<Subject> subjects){
        //("subject",JSONUtil.toJsonStr(subjects));
        redisUtil.set("subject",JSONUtil.toJsonStr(subjects));
    }
}
