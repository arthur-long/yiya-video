package com.yiya.video.service.impl;

import cn.hutool.core.text.UnicodeUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONUtil;
import com.yiya.video.config.YiYaConfig;
import com.yiya.video.modal.*;
import com.yiya.video.service.IvideoService;
import com.yiya.video.utill.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 视频接口实现类
 */
@Service
public class VideoServiceImpl implements IvideoService {

    @Autowired
    private RedisUtil redisUtil;

    @Override
    public String getNewVideoList() {
        return null;
    }

    @Override
    public String getSubJect() {
        String subject = (String)redisUtil.get("subject");
        if(StrUtil.isEmpty(subject)){
            return "";
        }
        return subject;
    }

    /**
     * 直接从api搜索
    * @param wd 关键字
     * @return
     */
    @Override
    public List<LastVideo>  getSearchVideo(String wd) {
        String data = HttpUtil.get(YiYaConfig.DO_MAIN + YiYaConfig.AC_LIST+YiYaConfig.PARM_WD+wd);
        String res = UnicodeUtil.toString(data);
        VideoPage videoPage = JSONUtil.toBean(res, VideoPage.class);
        List<LastVideo> videoList= videoPage.getList();
        return videoList;
    }

    /**
     * 根据类型id搜索
     * @param typeId
     * @return
     */
    @Override
    public List<LastVideo> getSearchVideoByTypeId(String typeId) {
        String data = HttpUtil.get(YiYaConfig.DO_MAIN + YiYaConfig.AC_LIST+YiYaConfig.PARM_TYPE+typeId);
        String res = UnicodeUtil.toString(data);
        VideoPage videoPage = JSONUtil.toBean(res, VideoPage.class);
        List<LastVideo> videoList= videoPage.getList();
        return videoList;
    }

    @Override
    public List<VideoDetail> getVideoDetail(String id) {
        String data = HttpUtil.get(YiYaConfig.DO_MAIN + YiYaConfig.AC_INFO+YiYaConfig.PARM_IDS+id);
        String res = UnicodeUtil.toString(data);
        VideoDetailPage videoPage = JSONUtil.toBean(res, VideoDetailPage.class);
         List<VideoDetail> details=videoPage.getList();
        //vod_play_url_list
        //"HD中字$https://iqiyi.cdn27-okzy.com/share/496bd33584d955e3913f1a3e82bb2f2d
        // $$$HD中字$https://iqiyi.cdn27-okzy.com/20210125/9103_b96da75c/index.m3u8"
        for(VideoDetail detail:details){
            String urls = detail.getVod_play_url();
            String[] urlArray = urls.split("\\$\\$\\$");
            if(urlArray.length>=1){
                String url = urlArray[0];
                String[] split = url.split("[$]");
                List<String> urlList = new ArrayList<>();
                for(String s:split){
                    if(s.indexOf("http")!=-1){
                        urlList.add(s);
                    }
                }
                detail.setVod_play_url_list(urlList);
            }
        }

        return details;
    }
}
