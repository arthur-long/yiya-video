package com.yiya.video.service;

import com.yiya.video.modal.LastVideo;
import com.yiya.video.modal.VideoDetail;

import java.util.List;

public interface IvideoService {
    public String getNewVideoList();
    public String getSubJect();
    public List<LastVideo> getSearchVideo(String wd);
    public List<LastVideo> getSearchVideoByTypeId(String typeId);
    public List<VideoDetail> getVideoDetail(String id);
}
