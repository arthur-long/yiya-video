# 咿呀视频

#### Description
咿呀科技有限公司研发视频共享软件-咿呀视频,采用源代码+app生成物全部共享模式，完全共享给每一个视频爱好者；该软件只允许个人学习使用，禁止商业使用，视频内容若存在侵权行文，请联系作者。使用过程中，若遇到任何侵权、法务等问题，请自行处理，与作者无关。

#### Software Architecture
Software architecture description
后台架构：SpringBoot
前端架构: uniApp
#### Installation

1.  将源代码挡下来，然后使用idea编译，将生成的jar包放到服务器上，java -jar XXXX.jar 
2.  使用HbuildX 打开YiYa-App,修改 request.js的域名，打包成APP即可

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request
![输入图片说明](https://images.gitee.com/uploads/images/2021/0203/091136_b2d675d0_4974180.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0203/091245_33c16004_4974180.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0203/091313_12e877dd_4974180.png "屏幕截图.png")
#### 赞助支持  - 一分也是情，一毛也是爱。 您的支持，是对作者最大的鼓励！
![输入图片说明](https://images.gitee.com/uploads/images/2021/0203/091430_caee7dd3_4974180.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0203/091503_e2898345_4974180.png "屏幕截图.png")

