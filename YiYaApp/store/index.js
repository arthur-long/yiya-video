import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
	state: {
		hasLogin: false,//用户是否登陆
		userInfo:{} //存放用户信息
	},
	mutations:{ //全局方法
		login(state,provider){
			state.hasLogin=true;
			state.userInfo=provider;
			uni.setStorage({
				key: 'userInfo',
				data:provider
			})
			console.log(state.userInfo);
		},
		logout(state){
			state.hasLogin=false;
			state.userInfo={};
			uni.removeStorage({
				key:'userinfo'
			})
		}
	}
})
export default store