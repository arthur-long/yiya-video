const api="http://hao.518gogo.com/video/api";

function sendRequest(url,method,data,header){
	var promise = new Promise(function(resolve,reject){
		uni.request({
			url:api+url,
			data:data,
			method:method,
			header:header,
			success(res) {
				resolve(res.data);
			},
			fail(res) {
				reject("网络异常");
			}
		})
	});
	return promise;
}

module.exports.sendRequest=sendRequest;